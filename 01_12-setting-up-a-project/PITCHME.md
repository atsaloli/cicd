---?image=img/new_project.png&position=bottom&size=auto 73%

## Setting up your CI/CD infrastructure
### Adding a project

Login as `root`. Add a project by selecting "New..." (plus sign) and "New project":

---

## Setting up your CI/CD infrastructure
### Adding a project

Name the project "www" (we'll pretend it's the source code
for our web site).

![name the project](img/name_project.png)

---
## Setting up your CI/CD infrastructure
### Adding a project
Change project Visibility Level from Private to Public (this will make it easier for us to set up Jenkins to pull from GitLab later) and select the green "Create project" button.

![create project](img/create_project.png)
---
## Setting up your CI/CD infrastructure
### Adding a project

GitLab will take you to the "www" project page, where you'll see the confirmation (in blue) that the project was created.

You'll also a prompt (in orange) to add an SSH key to your profile so you can pull or push project code.

Select "Don't show again" -- in this tutorial,
you'll use the GitLab Web UI to change files in your project.
![create project](img/project_successfully_created.png)
