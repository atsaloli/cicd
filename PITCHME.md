# Setting up CI/CD Pipelines
## with GitLab and Jenkins

Aleksey Tsalolikhin

aleksey@verticalsysadmin.com

30 Jan 2020

Pre-requisite: [Introduction to CI/CD: Concepts and Terminology](https://gitpitch.com/atsaloli/cicd-intro/master?grs=gitlab#/)

Tutorials:
- GitLab CI/CD
- Jenkins

---

# GitLab CI/CD Tutorial

Aleksey Tsalolikhin

aleksey@verticalsysadmin.com

30 January 2020

---

## Table of contents

- Introduction
- Setting up your CI/CD infrastructure
- Configuring CI/CD pipelines
- Troubleshooting
- Would you like to know more?

---

## Introduction

### GitLab

[GitLab](https://about.gitlab.com) is a Web application for working together on code. In addition to a Git data store, it has everything you need for a modern workflow: ticketing system, issue boards, code review, CI/CD, chat and more -- all smoothly integrated in one UI.

GitLab.com is the public service; the software is also available for private (on-prem) use in Libre (free of charge, open source) or Enterprise Edition (paid, with commerical support and added features).

---
## Introduction

### GitLab CI/CD (Runner Server)

GitLab comes with an add-on package called Runner Server which provides runners. Simply put, a runner is what runs the tests. Runners can run build jobs, test jobs, and deploy jobs. Or actually any kind of job! Runner Server works with GitLab through GitLab Server's API.

---?include=01_05-architecture/PITCHME.md

---

## Introduction

### Lab exercise

In this tutorial, you will set up:

- GitLab Server
- Runner Server
- UAT environment
- Production environment

Using these building blocks, you will set up a **CI/CD pipeline**:

![lab pipeline](img/lab-pipeline.png)

You will then re-use the UAT and Production environments in the Jenkins tutorial.

---

## Introduction

### Lab exercise

The pipeline could be fully automated:

![lab pipeline](img/lab-pipeline-full-auto.png)


---?include=01_10-installing-gitlab-server/PITCHME.md

---?include=01_12-setting-up-a-project/PITCHME.md

---?include=01_13-enabling-ci-on-a-project/PITCHME.md

---?include=01_20-installing-gitlab-ci/PITCHME.md

<!-- setting up runners -->

---?include=01_22-registering-our-first-runner/PITCHME.md

---?include=01_25-register-and-enable-Docker-runner/PITCHME.md

---?include=01_26-test-docker-runner/PITCHME.md

---?include=01_27-change-docker-image/PITCHME.md

<!-- setting up UAT and Prod environments -->
---?include=01_91-set-up-prod-and-stg-web-sites/PITCHME.md
---?include=01_92-deploy-using-ssh/PITCHME.md
---?include=01_93-deploy-via-git/PITCHME.md


---
## Configuring CI/CD pipelines
### Introduction

Now our infrastructure is set up. We have:

- GitLab Server (for our GitLab Web application)
- Runner Server (to provider runners, to run CI builds and tests, and CD jobs)
- a mock Stage environment (for User Acceptance Testing)
- a mock Prod environment

Trust relationships:
- `gitlab-runner` user can SSH to Stage environment
- `gitlab-runner` user can push to Git
- Prod environment can pull from Git

This next section will deal with configuring CI/CD pipelines.

It's purpose is to get you more familiar with GitLab's CI syntax and capabilities.

First, let's go over some key definitions.

---?include=02_00-key-definitions/PITCHME.md

<!-- Continuous Integration -->

---?include=02_01-single-test-job/PITCHME.md
---?include=02_02-simple-3-stage-shell-pipeline/PITCHME.md
---?include=02_03-simple-3-stage-docker-pipeline/PITCHME.md
---?include=02_05-parallel-jobs/PITCHME.md
---?include=02_30-custom-stages/PITCHME.md
---?include=02_40-testing-with-phpunit/PITCHME.md

<!-- Continuous Delivery -->
---?include=02_42-deploying-to-stage-via-ssh/PITCHME.md
---?include=02_44-set-up-git-pulls-from-mock-envs/PITCHME.md
---?include=02_45-deploying-via-git/PITCHME.md
---?include=02_50-manual-deploy-to-production/PITCHME.md

---
## Configuring CI/CD pipelines
### Continuous Deployment

This next section deals with Continuous Deployment (automated end-to-end).

---?include=02_55-continuous-deployment/PITCHME.md

<!-- Troubleshooting -->

---
## Troubleshooting
This next section deals with troubleshooting.

---?include=02_90-debugging-builds/PITCHME.md
---?include=02_100-increasing-loglevel/PITCHME.md
---?include=02_110-gitlab-logs/PITCHME.md
---?include=02_115-interactive-containers/PITCHME.md
---?include=02_118-env-vars/PITCHME.md
---?include=02_120-debugging/PITCHME.md

---?include=bookmarks/PITCHME.md

---
# Jenkins Tutorial

Aleksey Tsalolikhin

aleksey@verticalsysadmin.com

29 Oct 2018

---

## Table of Contents

- Overview and Architecture
- Jenkins Terms
- Installing and Configuring Jenkings
- Configuring Pipelines
- Checking Pipeline status with Jenkins Blue Ocean UI
- Troubleshooting

---
## Overview and Architecture

### Origin

> Jenkins was originally developed as the Hudson project [Sun Microsystems, 2004] ... Around 2007 Hudson became known as a better alternative to CruiseControl and other open-source build-servers.

-- [Wikipedia entry for Jenkins (software)](https://en.wikipedia.org/wiki/Jenkins_(software))

In 2011, Jenkins split off from Hudson following a dispute with Oracle.

---
### Architecture

- Jenkins is an open source automation server written in Java.
- Jenkins is free software (MIT license)
- There is a million plug-ins for Jenkins that can change its behavior or add functionality (1,799 plug-ins available as of Sep 21st, 2017)

---
## Jenkins Terms

> **Pipelines** are made up of multiple steps that allow you to build, test and deploy applications. Jenkins Pipeline allows you to compose multiple steps in an easy way that can help you model any sort of automation process.

> Think of a "**step**" like a single command which performs a single action. When a step succeeds it moves onto the next step. When a step fails to execute correctly the Pipeline will fail.

> When all the steps in the Pipeline have successfully completed the Pipeline is considered to have successfully executed. ...
>

> ... The **sh** step is used to execute a shell command in a Pipeline.
>
> The **agent** directive tells Jenkins where and how to execute the Pipeline, or subset thereof.

-- [Jenkins Guided Tour: Running Multiple Steps](https://jenkins.io/doc/pipeline/tour/running-multiple-steps/)

---

Example pipeline, this goes into a `Jenkinsfile` at the top of your project:

(Note: the Deploy step requires an SSH trust relationship which we haven't established yet.)

```
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                sh 'gcc hello.c -o hello'
            }
        }
        stage('Test') {
            steps {
                sh './hello | grep Hello'
            }
        }
        stage('Deploy') {
            steps {
                sh 'scp -i ~jenkins/.ssh/push_to_stg hello root@stage.example.com:/usr/local/bin'
            }
        }
    }
}

```

---
## More Jenkins Terms
From [Jenkins glossary](https://jenkins.io/doc/book/glossary/), we have:

- **Node**  A machine which is part of the Jenkins environment and capable of executing Pipelines or Projects.
- **Pipeline** A user-defined model of a continuous delivery pipeline
- **Project** A user-configured description of work which Jenkins should perform, such as building a piece of software, etc.
- **Agent** An agent is typically a machine, or container, which connects to a Jenkins master and executes tasks when directed by the master.
- **Master** The central, coordinating process which stores configuration, loads plugins, and renders the various user interfaces for Jenkins.
- **Stage** stage is part of Pipeline, and used for defining a conceptually distinct subset of the entire Pipeline, for example: "Build", "Test", and "Deploy", which is used by many plugins to visualize or present Jenkins Pipeline status/progress.
- **Step** A single task; fundamentally steps tell Jenkins what to do inside of a Pipeline or Project.
- **Workspace** A disposable directory on the file system of a Node where work can be done by a Pipeline or Project. Workspaces are typically left in place after a Build or Pipeline run completes unless specific Workspace cleanup policies have been put in place on the Jenkins Master.

---

## Set up Infrastructure
### Shut down Runner Server
Shut down Runner Server, as we'll be using Jenkins going forward.

```bash
sudo service gitlab-runner stop
```
---

## Set up Infrastructure
### Shut down GitLab

Shut down GitLab, as one of it's components listens on port 8080 which will conflict with Jenkins which listens on 8080 out of the box (we are going to change that):

```bash
sudo gitlab-ctl stop
```

---
## Set up Infrastructure
### Install Jenkins

```bash
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
echo 'deb https://pkg.jenkins.io/debian-stable binary/' | sudo tee -a /etc/apt/sources.list
sudo apt-get update
sudo apt-get install -y default-jre
sudo apt-get install -y jenkins

```
Reference: [installation instructions](https://jenkins.io/download/)
---

## Set up Infrastructure
### Change Jenkins port

By default, Jenkins listens on port 8080.

Change that to 8081 due to conflict with a GitLab component:

```bash
sudo service jenkins stop
sudo sed -i /etc/default/jenkins -e 's:HTTP_PORT=8080:HTTP_PORT=8081:'
sudo systemctl daemon-reload
sudo service jenkins start
sudo gitlab-ctl start

```

---

## Set up Infrastructure
### Set up SSH trust relationship

Allow Jenkins to deploy to Stage over SSH:

```bash
# generate an SSH key for Jenkins to push to Stage
sudo su - jenkins -c "mkdir .ssh; chmod 0700 .ssh; cd .ssh; ssh-keygen -f push_to_stg -N ''"

# add the key to Stage authorized keys
sudo cat ~jenkins/.ssh/push_to_stg.pub | sudo tee -a ~root/.ssh/authorized_keys

# Add Stage host key to Jenkins' known_hosts
sudo su - jenkins -c "ssh-keyscan -H stage.example.com >> ~jenkins/.ssh/known_hosts"
```
---
## Set up Infrastructure
### Test SSH trust relationship

```bash
sudo su - jenkins -c 'scp -i ~jenkins/.ssh/push_to_stg /etc/os-release  root@stage.example.com:/tmp/test.txt'
ls -lh /tmp/test.txt
```

---

## Set up Infrastructure
### Log in to Jenkins
Log in to Jenkins on port 8081, using the hostname you noted earlier, when you installed GitLab; and punch in the initial admin password, which you can find in:

```console
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```
---

## Set up Infrastructure
### Install Suggested Plugins

Select "Install Suggested Plugins".

Jenkins has a very rich plugin ecosystem: over 1,000 publicly listed plugins.

The [Plugins Index](https://plugins.jenkins.io) makes it easy to browse and search for plugins.

---

## Set up Infrastructure
### (Don't) Create First Admin User

You can skip the "create first admin user" form by selecting "continue as admin" at the bottom of the form.

In production, you'd want to use named accounts for administrators, for accountability; but for this tutorial, we'll stick with `admin`.

---

### Configure a pipeline in Jenkins

Select "New item", give it a name (e.g., "my-jenkins-pipeline") and 
a type (select "Multibranch Pipeline").
---
### Important note

If your GitLab project is private, you can make it Public so Jenkins
can access it:

Settings -> General -> Permission -> Expand -> Project Visibility -> Public

(or you can use deploy keys, which we haven't covered).

---
### Configure Git back-end

In the "Branch Sources" pull-down menu, select "Git".

Under "Project Repository", put the Git URL of your "www" project, e.g.,
http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git

Check the "Scan Multibranch Pipeline Triggers" box and set the Interval to 1 hour (these are small VMs and if you scan more often, like every minute, the VM will go unresponsive quite often).

Click Save, and you should see something like this: (next slide)

---
```text
Scan Multibranch Pipeline Log

Started
[Tue Oct 31 16:52:28 UTC 2017] Starting branch indexing...
 > git --version # timeout=10
 > git ls-remote http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git # timeout=10
Creating git repository in /var/lib/jenkins/caches/git-4e009f006aeb466fdf030686c9250da9
 > git init /var/lib/jenkins/caches/git-4e009f006aeb466fdf030686c9250da9 # timeout=10
Setting origin to http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git
 > git config remote.origin.url http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git # timeout=10
Fetching & pruning origin...
Fetching upstream changes from origin
 > git --version # timeout=10
 > git fetch --tags --progress origin +refs/heads/*:refs/remotes/origin/* --prune
Listing remote references...
 > git config --get remote.origin.url # timeout=10
 > git ls-remote -h http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git # timeout=10
Checking branches...
  Checking branch prod
      ‘Jenkinsfile’ not found
    Does not meet criteria
  Checking branch master
      ‘Jenkinsfile’ not found
    Does not meet criteria
Processed 2 branches
[Tue Oct 31 16:52:32 UTC 2017] Finished branch indexing. Indexing took 3.8 sec
Finished: SUCCESS
```
---
### Add Jenkinsfile

The log showed Jenkins is looking for `Jenkinsfile`.

Add a file called `Jenkinsfile` to the top of your project.

This file is like `.gitlab-ci.yml`, it consists of stages
(in sequence); with each stage made of jobs (which Jenkins
calls "steps").

It also contains the `agent` directive, which tells Jenkins where and how to execute the Pipeline, or subset thereof. More on this soon.

```
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                sh 'gcc hello.c -o hello'
            }
        }
        stage('Test') {
            steps {
                sh './hello | grep Hello'
            }
        }
        stage('Deploy') {
            steps {
                sh 'scp -i ~jenkins/.ssh/push_to_stg hello root@stage.example.com:/usr/local/bin'
            }
        }
    }
}

```

---

We are going to need a `hello.c` for this demo.

Add a file `hello.c` to the top of our project, we'll use it for initial
testing of our Jenkins pipeline:

```c
# include <stdio.h>
main()
{
    printf("Hello World\n");
}
```

---

Select "Scan Multibranch Pipeline Now" after adding the `Jenkinsfile`, and look at the scan log:

```text
Started by user admin
[Tue Oct 31 16:55:28 UTC 2017] Starting branch indexing...
 > git --version # timeout=10
 > git ls-remote http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git # timeout=10
 > git rev-parse --is-inside-work-tree # timeout=10
Setting origin to http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git
 > git config remote.origin.url http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git # timeout=10
Fetching & pruning origin...
Fetching upstream changes from origin
 > git --version # timeout=10
 > git fetch --tags --progress origin +refs/heads/*:refs/remotes/origin/* --prune
Listing remote references...
 > git config --get remote.origin.url # timeout=10
 > git ls-remote -h http://ec2-18-195-25-169.eu-central-1.compute.amazonaws.com/root/www.git # timeout=10
Checking branches...
  Checking branch prod
      ‘Jenkinsfile’ not found
    Does not meet criteria
  Checking branch master
      ‘Jenkinsfile’ found
    Met criteria
Scheduled build for branch: master
Processed 2 branches
[Tue Oct 31 16:55:29 UTC 2017] Finished branch indexing. Indexing took 0.46 sec
Finished: SUCCESS
```
You can see Jenkins found `Jenkinsfile` in the "master" branch and scheduled a build.

---

Select "Build History" (in the left-hand nav bar) and you can see the build failed.

If you select the broken build (#1), and then select "Console Output",
you can see just where the pipeline failed:

```text
[Pipeline] stage
[Pipeline] { (Build)
[Pipeline] sh
[shiny_master-673BQMYXMXJNV375U2SCGTPEIJ7N3PR7ZX2JOVAXNW5GYCENX4NA] Running shell script
+ gcc hello.c -o hello
/var/lib/jenkins/workspace/shiny_master-673BQMYXMXJNV375U2SCGTPEIJ7N3PR7ZX2JOVAXNW5GYCENX4NA@tmp/durable-2b815199/script.sh: 2: /var/lib/jenkins/workspace/shiny_master-673BQMYXMXJNV375U2SCGTPEIJ7N3PR7ZX2JOVAXNW5GYCENX4NA@tmp/durable-2b815199/script.sh: gcc: not found
[Pipeline] }
```
---
Actually, we don't want to run build jobs in the shell, do we?

Builds should be reproducible, so let's move this activity to a container.

Go to "Manage Jenkins" -> "Manage Plugins" -> "Available", and search for Docker.

Voila! "Docker plugin" from https://plugins.jenkins.io/docker-plugin

> Docker plugin allows to use a docker host to dynamically provision build agents, run a single build, then tear-down agent.

Select the checkbox next to "docker-plugin" and select "Download now and install after restart", and on the next screen, check the box to "Restart Jenkins when no jobs are running".

---
### Allow Jenkins to use Docker


```bash
sudo usermod -aG docker jenkins
sudo service jenkins restart
```
---
### Configure Jenkins to use Docker


Go to "Manage Jenkins" -> "Configure System" -> "Cloud" -> "Add a new cloud" -> "Docker"

For name, use "my-docker-cloud".

Expand "Docker cloud details". For "Docker Host URI", use `unix:///var/run/docker.sock`

Select "Test connection" and Jenkins should connect to the Docker API and return the version of the API.

Check "Enabled".

Select "Save".

---

### Dockerfile

Add a `Dockerfile` file to the top of our project:

```
# vim:set ft=dockerfile:
FROM ubuntu:latest

RUN apt-get update \
        && apt-get install -y build-essential

```


---
### Configure Jenkinsfile to use Dockerfile

```
pipeline {
    agent none

    stages {
        stage('Build') {
            agent { dockerfile true }
            steps {
                sh 'cat /etc/*release'
                sh 'gcc hello.c -o hello'
            }
        }
        stage('Test') {
            agent { dockerfile true }
            steps {
                sh './hello | grep Hello'
            }
        }
        stage('Deploy') {
            agent any
            steps {
                sh 'scp -i ~jenkins/.ssh/push_to_stg hello root@stage.example.com:/usr/local/bin'
            }
        }
    }
}


```
---
### Manual trigger

How do we set up a manual trigger, similar to the "Play" button in GitLab?

Try running this `Jenkinsfile` and watch the Console Output in the Jenkins UI:

```
pipeline {
    agent any
    stages {
        /* "Build" and "Test" stages omitted */

        stage('Deploy - Staging') {
            steps {
                sh 'echo deploy staging'
                sh 'echo run-smoke-tests'
            }
        }

        stage('Sanity check') {
            steps {
                input "Does the staging environment look ok?"
            }
        }

        stage('Deploy - Production') {
            steps {
                sh 'echo /deploy production'
            }
        }
    }
}
```
---

### Blue vs Green

You may have noticed the balls and progress bars are blue instead of green, when everything is OK.

The creator of Jenkins, Kohsuke Kawaguchi, is Japanese, and in Japan, green is considered a shade of blue.

If this bugs you, install the ["Green Balls"](https://wiki.jenkins.io/display/JENKINS/Green+Balls) plugin to turn the blue balls green.

References:
- https://jenkins.io/blog/2012/03/13/why-does-jenkins-have-blue-balls/
- http://www.atlasobscura.com/articles/japan-green-traffic-lights-blue
---

## Checking Pipeline status with Jenkins Blue Ocean UI

Install the "Blue Ocean" plug-in (Manage Jenkins -> Manage Plugins).

Once it installs and Jenkins restarts, you can select "Open Blue Ocean" in the main menu, and you will see a visual representation of your Pipelines and Projects.

---
## Troubleshooting

- Add /log to your Jenkins UI URL, e.g. http://jenkins.example.com/log
- Take a look at `/var/log/jenkins/jenkins.log`
- Install the [monitoring](https://wiki.jenkins.io/display/JENKINS/Monitoring) plug-in and visit /monitoring at your Jenkins UI

Reference: http://www.scmgalaxy.com/tutorials/jenkins-troubleshooting

---

## Finished early?

Finished early?

Go to https://gitlab.com/atsaloli/gitlab-ci-tutorial/blob/master/gitlab-ci/README.md for bonus GitLab content.

GitLab CI YAML full reference is at https://docs.gitlab.com/ce/ci/yaml/README.html
