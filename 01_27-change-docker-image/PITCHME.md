## Setting up your CI/CD infrastructure
### Changing container image

Change your CI config to tell the Docker runner to use an Ubuntu container image for all jobs for this project:

```yaml
image: ubuntu

test_it:
  script: cat /etc/*release
  tags:
  - docker
```

Confirm in the job log that Runner Server is creating an Ubuntu container.
---

## Setting up your CI/CD infrastructure
### Changing container image per job

Not only can you specify the container image globally, you can override
the image definition _per job_:

```yaml
image: ubuntu
  
test_it:
  script: cat /etc/*release
  tags:
  - docker
  
alpine_test:
  image: alpine
  script: cat /etc/*release
  tags:
  - docker

```

Make the above change (to `.gitlab-ci.yml`) and check the logs for each job, with attention to what image was used in each.
