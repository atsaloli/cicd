---

## Setting up your CI/CD infrastructure
### Configuring CI

GitLab CI lives in `.gitlab-ci.yml`.

There can be one `.gitlab-ci.yml` file per project and it lives at the top level.

References:
- [GitLab CI configuration syntax](https://docs.gitlab.com/ce/ci/yaml/README.html)
- [YAML syntax](http://yaml.org)

---

## Setting up your CI/CD infrastructure
### Configuring CI

To setup CI, create the GitLab CI config file, `.gitlab-ci.yml`.

Select "New file":

![new file](img/new_file.png)

---

## Setting up your CI/CD infrastructure
### Configuring CI

Put `.gitlab-ci.yml` for the file name:

![name the new file](img/name_gitlab_ci_yml.png)

---

## Setting up your CI/CD infrastructure
### Configuring CI

Put the following for the file body:


```console
test_it:
  script: /bin/echo I am a pretend test suite. I passed!
```

![img](img/pretend_test_1.png)


---

## Setting up your CI/CD infrastructure
### Configuring CI


Select the green "Commit changes" button at the bottom.

---

## Setting up your CI/CD infrastructure
### Configuring CI

The `script` directive lists the command that the GitLab test runner will run to test your code.

```console
test_it:
  script: /bin/echo I am a pretend test suite. I passed!
```
---

## Setting up your CI/CD infrastructure
### Configuring CI

The `script` directive can contain a list of commands:

```console
test_it:
  script:
  - cmd1
  - cmd2
  - cmd3
```
---


## Setting up your CI/CD infrastructure
### Configuring CI

By default, GitLab runs CI on every commit,
to catch problems early.

It _is_ possible to run CI jobs on specific branches
only, e.g., only on merge requests into the "master" branch.
(However, that's outside the scope of this introductory tutorial.)



---
## Setting up your CI/CD infrastructure
### Configuring CI
Go to "CI/CD -> Pipelines" to see pipeline status:

![pipelines menu](img/new_cicd_pipelines_menu.png)

---
## Setting up your CI/CD infrastructure
### Configuring CI
You'll see the pipeline is "pending" and that it's "stuck":

![stuck pipeline](img/stuck_pipeline.png)

Let's setup our CI/CD server yet.
