## Setting up your CI/CD infrastructure

### Introducing Runner Server

There is a a separate package we have to install 
to add the CI component to GitLab. 

This component is called Runner Server.

Runner Server will pick up and process pending CI jobs.

---
## Setting up your CI/CD infrastructure

### Installing Runner Server

<!--
The following was the old text for this slide -- it does not work anymore since Ubuntu repo now has a "gitlab-runner" package
which has a name collision with GitLab's "gitlab-runner" package (I even tried the APT pinning hack but it didn't work):

GitLab has a dedicated package repository for Runner Server packages.

Add the repo definition and install the Runner Server package:


```console
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash

sudo apt-get install -y gitlab-ci-multi-runner
```
-->

Download and install the latest 64-bit Debian package of GitLab Runner from GitLab:


```console
curl https://gitlab-runner-downloads.s3.amazonaws.com/latest/\
deb/gitlab-runner_amd64.deb
sudo dpkg -i ./gitlab-runner_amd64.deb
```

---

## Setting up your CI/CD infrastructure
### Confirm Runner Server is up

Check service status:

```console
sudo service gitlab-runner status
```

![runner service is active](img/runner_service_active.png)
---

## Setting up your CI/CD infrastructure
### Installing Docker

Let's install Docker, to run repeatable tests in reproducible environments.

Do this on the Runner Server, which, for expediency in this tutorial,
is the same as the GitLab Server. For production, you
should have dedicated Runner Servers and a dedicated GitLab Server.

Install and test Docker:

```console
curl -sSL https://get.docker.com/ | sudo sh

sudo docker run alpine /bin/echo 'Hello world'
```

Example successful test:

```shell_session
ubuntu@1ddb8d03-6597-48bd-a001-9c4f5a99dd6d:~$ sudo docker run alpine /bin/echo 'Hello world'
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
88286f41530e: Pull complete
Digest: sha256:f006ecbb824d87947d0b51ab8488634bf69fe4094959d935c0c103f4820a417d
Status: Downloaded newer image for alpine:latest
Hello world
ubuntu@1ddb8d03-6597-48bd-a001-9c4f5a99dd6d:~$
```
---
## Setting up your CI/CD infrastructure
### Configure `docker` group

Allow non-root user "gitlab-runner" to use Docker:

```console 
sudo usermod -aG docker gitlab-runner

```

Note: By adding `gitlab-runner` to the `docker` group you are effectively granting `gitlab-runner` full root permissions. See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html for alternatives and their tradeoffs.

---

## Setting up your CI/CD infrastructure
### Locate the config file for Runner Server

Runner Server's config file is `/etc/gitlab-runner/config.toml`.

TOML is [Tom's Obvious, Minimal Language](https://github.com/toml-lang/toml), easier (for humans) than YAML:

```console
sudo cat /etc/gitlab-runner/config.toml
```

Example:

```text
concurrent = 1
check_interval = 0
```

| Setting | Description |
|---------|-------------|
| concurrent | Limits how many jobs globally can be run concurrently. The most upper limit of jobs using all defined runners. 0 does not mean unlimited |
| check_interval | defines in seconds how often to check GitLab for a new builds |

We won't look at the [other possible settings](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/configuration/advanced-configuration.md).
---

## Setting up your CI/CD infrastructure
### Increase concurrency

Edit `/etc/gitlab-runner/config.toml` to increase concurrency to 3.

```console
sudo vim /etc/gitlab-runner/config.toml
```
Runner Server will pick up the change automatically.

Now we can run jobs in parallel (provided there are no dependencies).
