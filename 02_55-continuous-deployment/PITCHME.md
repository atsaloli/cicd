## Configuring CI/CD pipelines
### Continuous Deployment

Use the "x" shortcut to highlight the entire YAML in this slide (it won't all show up in the slide), then paste it into an editor and examine it:

```yaml
stages:
  - test
  - deploy_to_stage
  - test_on_stage
  - deploy_to_prod

preflight_test:
  tags:
  - docker
  image: ubuntu
  script:
  - apt-get update
  - DEBIAN_FRONTEND=noninteractive apt-get install -y phpunit
  - phpunit UnitTest HelloTest.php

rsync_to_stage:
  tags:
    - shell
  stage: deploy_to_stage
  script:
  - rsync -av -e 'ssh -i ~gitlab-runner/.ssh/push_to_stg_docroot' *.php root@stage.example.com:/var/www/stg-html/
  environment:
    name: stage
    url: http://stage.example.com:8008/

functional_test:
  stage: test_on_stage
  script: curl http://stage.example.com:8008/ | grep Hello
  tags:
  - shell

push_to_prod_branch:
  stage: deploy_to_prod
  tags:
    - shell
  script:
  - GIT_SSH_COMMAND="ssh -i ~gitlab-runner/.ssh/push_to_git" git push --force git@gitlab.example.com:root/www.git +HEAD:refs/heads/prod
  environment:
    name: production
    url: http://prod.example.com:8008/
```
---
## Configuring CI/CD pipelines
### Continuous Deployment

Run the pipeline from the preceeding slide and observe the different stages and jobs in each stage.

Now change `$my_greeting` in `Hello.php` from "Hello world" to "Hello everyone".

What do you predict will happen when the pipeline runs? Observe what happens and see if you predicted correctly. 
