## Setting up your CI/CD infrastructure

### Installing GitLab Server

#### Creating a host

During the tutorial, you'll be provided a [Strigo](https://strigo.io/)
VM in the AWS public cloud. 

Your Strigo VM will be destroyed after the workshop.

To continue your study after workshop, you can use a host in a public
cloud. All you need is:

- a VM (that can run Docker)
- Ubuntu 18 LTS
- 8 GB of RAM (4 GB for GitLab Server, and 4 GB for GitLab Runner)

Note: Make sure HTTP inbound is open, so you can access the GitLab Web UI.

---
## Setting up your CI/CD infrastructure

### Installing GitLab Server

#### Strigo VM

Event URL: 
 
Code: 

---
## Setting up your CI/CD infrastructure

### Installing GitLab Server

#### Strigo Web terminal: Copying and Pasting

On the Mac, you can use the usual hotkeys (Command-C to copy and Command-V to paste).

On other systems:
- Copying: highlight the text, right-click in the Web browser and select "Copy" from the menu.
- Pasting: use Ctrl-Shift-V, or right-click and select "Paste".

---
## Setting up your CI/CD infrastructure

### Installing GitLab Server

#### Community Edition vs Enterprise Edition

We'll install GitLab EE (Enterprise Edition) which is functionally
identical to GitLab Libre (formerly called CE, or Community Edition)
until you install an EE license to enable the additional features.
This makes upgrading to EE a breeze.

We'll use the official package from GitLab.  GitLab releases a
new version on the 22nd of every month -- and always with new
features!
---
## Setting up your CI/CD infrastructure

### Installing GitLab Server

#### Add GitLab repository definition and install GitLab EE package

```console
curl https://packages.gitlab.com/install/repositories/gitlab/\
gitlab-ee/script.deb.sh | sudo bash
sudo apt-get install gitlab-ee 
```

Pro tip: Use "x" in your Web browser to select the code block above.

Note - The above is just enough to
get GitLab up for our tutorial. The full installation instructions are at
[https://about.gitlab.com/installation](https://about.gitlab.com/installation/) 
(e.g., if you need to set up HTTPS or outgoing email for notifications, it's all covered there.)
---
## Setting up your CI/CD infrastructure
### Installing GitLab Server
#### Note your GitLab URL

The installer will print the URL of your GitLab Server instance below the
GitLab _tanuki_.

The _tanuki_ is also known as the Asiatic racoon, or racoon dog.
It's a member of the dog family.

You can see a picture of the tanuki [here](https://commons.wikimedia.org/wiki/File:Tanuki01_960.jpg).

---
GitLab adopted the tanuki as its mascot because the tanuki is smart, and works in a group
to achieve a comman goal -- and thus symbolizes GitLab vision of enabling collaboration.

![GitLab tanuki logo](img/gitlab-logo-gray-rgb.svg)

---
## Setting up your CI/CD infrastructure
### Installing GitLab Server
#### Note your GitLab URL

Your GitLab URL will be under the tanuki ASCII art:

```text
       *.                  *.
      ***                 ***
     *****               *****
    .******             *******
    ********            ********
   ,,,,,,,,,***********,,,,,,,,,
  ,,,,,,,,,,,*********,,,,,,,,,,,
  .,,,,,,,,,,,*******,,,,,,,,,,,,
      ,,,,,,,,,*****,,,,,,,,,.
         ,,,,,,,****,,,,,,
            .,,,***,,,,
                ,*,.



     _______ __  __          __
    / ____(_) /_/ /   ____ _/ /_
   / / __/ / __/ /   / __ \`/ __ \
  / /_/ / / /_/ /___/ /_/ / /_/ /
  \____/_/\__/_____/\__,_/_.___/


Thank you for installing GitLab!
GitLab should be available at http://ec2-52-59-79-192.eu-central-1.compute.amazonaws.com
```

---
## Setting up your CI/CD infrastructure
### Installing GitLab Server

![important](img/important-one-tenth.png)  
Note your GitLab URL, you *will* need it later.

---
## Setting up your CI/CD infrastructure
### Installing GitLab Server

![important](img/important-one-tenth.png)  
Did you note your GitLab URL?

---?image=img/login.png&position=bottom&size=auto 85%

Go to your GitLab instance and set the password for `root`, the initial user.
